# JUAN JONES - CSC299 FINAL PROJECT
#!/usr/bin/python3

import json
import hashlib
import sys
from web3 import Web3, HTTPProvider, IPCProvider
from pprint import pprint

web3 = Web3 (HTTPProvider ("http://localhost:9545"))

# Read the ABI
with open ("abi.json") as f:
    abi = json.load (f)

# Get the contract address
if len (sys.argv) == 2:
    contract_address = sys.argv[1]
else:
    block_number = web3.eth.blockNumber
    contract_address = None
    while contract_address == None and block_number >= 0:
        block = web3.eth.getBlock (block_number)
        for tx_hash in block.transactions:
            tx = web3.eth.getTransactionReceipt (tx_hash)
            contract_address = tx.get ("contractAddress") 
            if contract_address != None:
                break
        block_number = block_number - 1
contract = web3.eth.contract (abi = abi, address = contract_address)
# Lists storing account balances through the lottery process
initial=[]
middle=[]
final=[]

def main ():
    print("\n{:s} LOTTERY {:s}\n".format("#"*18, "#"*18))
    print ("Using contract address {:s}\n".format (contract_address))
    printInitial()
    while True:
        account = eval(input("\nWhat index account is going to play? "))
        n = eval(input("Enter a number to win (0 to 255): "))
        r = eval(input("Enter a random number for hashing (0 to 255): "))
        play(account, n, r)
        again = input("\nPlay again? Y/N ")
        if (again == "y" or again == "Y"): continue
        else: break
    printMiddle()
    winningNumber(eval(input("\nEnter the winning number: ")))
    while True:
        account = eval(input("\nWhat index account is going to reveal? "))
        r = eval(input("Reveal random number: "))
        reveal(account, r)
        again = input("\nReveal another account? Y/N ")
        if (again == "y" or again == "Y"): continue
        else: break
    endLottery()

def play(account_index, n, r):
    account = web3.eth.accounts[account_index]
    data = int.to_bytes (n, 32, "big") + int.to_bytes (r, 32, "big")
    hash_nr = hashlib.sha256 (data).hexdigest ()
    transaction_hash = contract.transact ({
    "from": account,
    "value": web3.toWei (1, "ether")
    }).play (Web3.toBytes (hexstr = hash_nr));
    print ("Using account {:d} with address {:s} to play on contract {:s}".format (account_index, account, contract_address))

def winningNumber(n):
    account = web3.eth.accounts[0]
    transaction_hash = contract.transact ({
    "from": account
    }).winningChoice (n);
    print("\n ** The winning number is {:d} **".format(n))

def reveal(account_index, r):
    account = web3.eth.accounts[account_index]
    transaction_hash = contract.transact ({
    "from": account
    }).reveal (r);
    print ("Using account {:d} with address {:s} to reveal on contract {:s}".format (account_index, account, contract_address))

def endLottery():
    account = web3.eth.accounts[0]
    transaction_hash = contract.transact ({
    "from": account
    }).endLottery ();
    printFinal()
    for i in range(0, len(final)):
        difference = final[i]-initial[i]
        payed = initial[i] - middle[i]
        award = final[i]-middle[i]
        if (difference > 0):
            print ("Account {:d} won {:.020f} ETH".format(i, difference))
            if (award>0):
                print ("(Paid {:.020f} to play and got {:.020f} ETH back)\n".format(payed, award))
            else:
                print ("(Paid {:.020f} to play and {:.020f} ETH for GAS)\n".format(payed, ans(award)))
        elif (difference ==0):
            print ("Account {:d} did not play\n".format(i))
        else:
            print ("Account {:d} lost {:.020f} ETH".format(i, difference))
            if (award>0):
                print ("(Paid {:.020f} to play and got {:.020f} ETH back)\n".format(payed, award))
            else:
                print ("(Paid {:.020f} to play and {:.020f} ETH for GAS)\n".format(payed, abs(award)))
    print()
    
def printInitial():
    count = 0
    print ("Initial Balances")
    for acc in web3.eth.accounts:
        balance = web3.eth.getBalance (acc)
        initial.append(float (web3.fromWei (balance, "ether")))
        count +=1
        print ("{:s} has {:.020f} ETH".format (acc, float (web3.fromWei (balance, "ether"))))

def printMiddle():
    count = 0
    print ("\nBalances after playing")
    for acc in web3.eth.accounts:
        balance = web3.eth.getBalance (acc)
        middle.append(float (web3.fromWei (balance, "ether")))
        count += 1
        print ("{:s} has {:.020f} ETH".format (acc, float (web3.fromWei (balance, "ether"))))
        
def printFinal():
    count = 0
    print ("\nFinal Balances")
    for acc in web3.eth.accounts:
        balance = web3.eth.getBalance (acc)
        final.append(float (web3.fromWei (balance, "ether")))
        count += 1
        print ("{:s} has {:.020f} ETH".format (acc, float (web3.fromWei (balance, "ether"))))
    print()

main()
