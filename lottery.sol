pragma solidity ^0.4.20;

// JUAN JONES - CSC299 Final Project

contract Lottery {
  address public owner;
  uint public pickingEnd;
  uint public revealingEnd;
  uint256 public winningNumber;
  bool winnerNumberAssigned = false;
  address [] public winners;
  
  mapping (address => bytes32) played;

  function Lottery (uint lotteryTime, uint revealingTime) public {
    owner = msg.sender;
    pickingEnd = now + lotteryTime;
    revealingEnd = pickingEnd + revealingTime;
  }
  
  function play (bytes32 choice) external payable {
    //require (now <= pickingEnd);
    require(msg.value == 1 ether);
    require(msg.sender!=owner);
    played[msg.sender] = choice;
  }

  function winningChoice (uint256 ownerNumber) external {
    require (!winnerNumberAssigned);
    require (msg.sender == owner);
    //require (now > pickingEnd);
    winningNumber = ownerNumber;
    winnerNumberAssigned = true;
  }

  function reveal (uint256 randomNumber) external {
    //require (now <= revealingEnd);
    require (winnerNumberAssigned);
    bytes32 choice = sha256(winningNumber, randomNumber);
    require (choice == played[msg.sender])
    winners.push(msg.sender);
  }

  function endLottery () external {
    //require (now > revealingEnd);
    require (msg.sender == owner);
    if (winners.length == 0) {
      owner.transfer(this.balance);
    }
    else {
      uint256 award = this.balance/winners.length;
      for (uint i=0; i<winners.length; i++ ) 
        winners[i].transfer(award);
    }
    selfdestruct(owner);
  }
}